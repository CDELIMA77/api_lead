package com.itau.lead.collector.services;

import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;

@SpringBootTest
public class ProdutoServicesTest {

        @MockBean
        ProdutoRepository produtoRepository;

        @Autowired
        ProdutoServices produtoServices;

        Produto produto;

        @BeforeEach
        public void inicializarTest(){
            produto = new Produto();
            produto.setIdp(1);
            produto.setNome("Cafe");
            produto.setDescricao("Cafe organico");
            produto.setPreco(20.00);
        }

        @Test
        public void testarSalvarProduto() {
            Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

            Produto produtoObjeto  = produtoServices.salvarProduto(produto);

            Assertions.assertEquals(produto, produtoObjeto);
            Assertions.assertEquals(produto.getNome(), produtoObjeto.getNome());

        /* Sempre programar pra dar certo e errado, este abaixo faz quebrar
            Assertions.assertEquals("Macarrão", produto.getNome()); */
        }


        @Test
        public void testarDeletarProduto() {
            produtoServices.deletarProduto(produto);
            Mockito.verify(produtoRepository,Mockito.times(1)).delete(Mockito.any(Produto.class));
        }


        @Test
        public void testarAtualizarProduto() {

            String nomeOriginal = produto.getNome();
            Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

            produto.setNome("Açucar");
            Produto produtoObjeto2  = produtoServices.atualizarProduto(produto);

            /* Teste do que mudou */
            Assertions.assertEquals("Açucar", produtoObjeto2.getNome());
            /* Teste do que não mudou */
            Assertions.assertEquals(20.00, produtoObjeto2.getPreco());

            /*Falha
            Assertions.assertEquals(nomeOriginal, produtoObjeto2.getNome());*/

        }

        @Test
        public void testarbuscarTodosProdutos() {

            produtoServices.buscarTodosProdutos();

            Assertions.assertEquals("Cafe", produto.getNome());

        }


    }



