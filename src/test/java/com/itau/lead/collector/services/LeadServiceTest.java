package com.itau.lead.collector.services;

import com.itau.lead.collector.enums.TipoDeLead;
import com.itau.lead.collector.models.Lead;
import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.repositories.LeadRepository;
import com.itau.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    /* Objeto simulado para testes, para não interferir com a produçao */
    LeadRepository leadRepository;

    @Autowired
    LeadServices leadServices;

    Lead lead;

    @BeforeEach
    public void inicializarTest(){
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("cynthia@gmail.com");
        lead.setNome("CynthiaLima");
        lead.setProdutos(Arrays.asList(new Produto()));
    }

    @Test
    public void testarSalvarLead() {
        /*Captura a chamada do método e simula o retorno */
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        /* Aqui que estamos chamando a classe a ser testada */
        Lead leadObjeto  = leadServices.salvarLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());

        /* Sempre programar pra dar certo e errado, este abaixo faz quebrar
        Assertions.assertEquals(lead.getEmail(), "xablau@gmail.com"); */
    }


    @Test
    public void testarDeletarLead() {
        /*
        List< Lead> leads = new ArrayList<>();
        leads.add(lead);

        lead = new Lead();
        lead.setId(2);
        lead.setTipoDeLead(TipoDeLead.FRIO);
        lead.setEmail("fabio@gmail.com");
        lead.setNome("FabioLever");
        lead.setProdutos(Arrays.asList(new Produto()));
        leads.add(lead);
        int tam = leads.size();

        leadServices.deletarLead(lead);
        int tam2 = leads.size();

        Assertions.assertEquals(tam, tam2 + 1);

        se quiser fazer assim precisa agora usar um lamba pra remover da lista */


        leadServices.deletarLead(lead);
        Mockito.verify(leadRepository,Mockito.times(1)).delete(Mockito.any(Lead.class));
    }


    @Test
    public void testarAtualizarLead() {

        String emailOriginal = lead.getEmail();
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadServices.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);

        lead.setEmail("cynthialima@gmail.com");
        Lead leadObjeto2  = leadServices.atualizarLead(lead);

        /* Teste do que mudou */
        Assertions.assertEquals("cynthialima@gmail.com", leadObjeto2.getEmail());
        /* Teste do que não mudou */
        Assertions.assertEquals("CynthiaLima", leadObjeto2.getNome());

        /*Falha
        Assertions.assertEquals(emailOriginal, leadObjeto2.getEmail());
        Assertions.assertEquals("cynthia@gmail.com",leadObjeto2.getEmail());*/
    }

    @Test
    public void testarbuscarTodosLeads() {
        /* Aqui que estamos chamando a classe a ser testada */
        leadServices.buscarTodosLeads();
        Assertions.assertEquals("CynthiaLima", lead.getNome());
    }

    @Test
    public void testarbuscarPorId() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        String nome = leadServices.buscarPorId(1).get().getNome();
        Assertions.assertEquals("CynthiaLima", nome);

        Mockito.verify(leadRepository,Mockito.times(1)).findById(1);
    }
}
