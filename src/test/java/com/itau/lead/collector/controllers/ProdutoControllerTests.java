package com.itau.lead.collector.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.lead.collector.models.Lead;
import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.services.LeadServices;
import com.itau.lead.collector.services.ProdutoServices;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
/* Preparar a classe para realizar simulação de requisição web */

public class ProdutoControllerTests {

        @MockBean
        LeadServices leadServices;

        @MockBean
        ProdutoServices produtoServices;

        @Autowired
        private MockMvc mockMvc;
        /* simula chamadas no nosso controller , configurar e criar requisições simuladas para o endpoint */

        Lead lead;
        Produto produto;

        @BeforeEach
        public void inicializar(){
            produto = new Produto();
            produto.setNome("Cafe");
            produto.setIdp(1);
            produto.setDescricao("Cafe organico");
            produto.setPreco(20.00);
        }

        /*Converte objeto java para json*/
        ObjectMapper mapper = new ObjectMapper();

        @Test
        public void testarIncluirProduto() throws Exception {
            Mockito.when(produtoServices.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);
            String json = mapper.writeValueAsString(produto);

            /* Até aqui somente a requisição foi preparada, agora que começa o teste */
            mockMvc.perform(MockMvcRequestBuilders.post("/Produto")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                    .andExpect(MockMvcResultMatchers.status().isCreated())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.idp",
                            CoreMatchers.equalTo(1)))  ;
        }

    @Test
    public void testarAtualizarProduto() throws Exception {
        String nomeOriginal = produto.getNome();
        produto.setNome("Chocolate");

        Mockito.when(produtoServices.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);
        String json = mapper.writeValueAsString(produto);

        /* Até aqui somente a requisição foi preparada, agora que começa o teste */
        mockMvc.perform(MockMvcRequestBuilders.put("/Produto/1").contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Chocolate")))
        ;
        }


    @Test
        public void testarDeletarProduto() throws Exception {
            Optional<Produto> produtoOptional = Optional.of(this.produto);

            Mockito.when(produtoServices.buscarPorIdp(Mockito.anyInt())).thenReturn(produtoOptional);
            String json = mapper.writeValueAsString(produto);

            /* Até aqui somente a requisição foi preparada, agora que começa o teste */
            mockMvc.perform(MockMvcRequestBuilders.delete("/Produto/1").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isNoContent())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.idp", CoreMatchers.equalTo(1))) /*compara resposta JSON com o o que eu programar */ ;

            Mockito.verify(produtoServices,Mockito.times(1)).deletarProduto(Mockito.any(Produto.class));
        }

        @Test
        public void testarBuscarProduto() throws Exception{
            Optional<Produto> produtoOptional = Optional.of(produto);
            Mockito.when(produtoServices.buscarPorIdp(Mockito.anyInt())).thenReturn(produtoOptional);

            Iterable<Produto> produtoIterable = Arrays.asList(produto);

            mockMvc.perform(MockMvcRequestBuilders.get("/Produto/1")).
                    andExpect(MockMvcResultMatchers.status().isOk());

            /*Falha não está funcionando*/
            mockMvc.perform(MockMvcRequestBuilders.get("/Produto/3"))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        }


    @Test
    public void testarBuscarTodosProdutos() throws Exception{
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoServices.buscarTodosProdutos()).thenReturn(produtoIterable);


        mockMvc.perform(MockMvcRequestBuilders.get("/Produto/")).
                andExpect(MockMvcResultMatchers.status().isOk());

        /*Falha não está funcionando*/
        /*mockMvc.perform(MockMvcRequestBuilders.get("/Lead/3"))
                .andExpect(MockMvcResultMatchers.status().isOk());     */
    }
}
