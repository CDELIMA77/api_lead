package com.itau.lead.collector.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.lead.collector.enums.TipoDeLead;
import com.itau.lead.collector.models.Lead;
import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.services.LeadServices;
import com.itau.lead.collector.services.ProdutoServices;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
/* Preparar a classe para realizar simulação de requisição web */

public class LeadControllerTests {

    @MockBean
    LeadServices leadServices;

    @MockBean
    ProdutoServices produtoServices;

    @Autowired
    private MockMvc mockMvc;
    /* simula chamadas no nosso controller , configurar e criar requisições simuladas para o endpoint */

    Lead lead;
    Produto produto;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Cynthia Carvalho de Lima");
        lead.setEmail("cynthia@gmail.com");
        lead.setTipoDeLead(TipoDeLead.FRIO);

        produto = new Produto();
        produto.setNome("Cafe");
        produto.setIdp(1);
        produto.setDescricao("Cafe organico");
        produto.setPreco(20.00);

        lead.setProdutos(Arrays.asList(produto));
   }

    /*Converte objeto java para json*/
    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testarIncluirLead() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);

        Mockito.when(leadServices.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadServices.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = mapper.writeValueAsString(lead);

        /* Até aqui somente a requisição foi preparada, agora que começa o teste */
        mockMvc.perform(MockMvcRequestBuilders.post("/Lead")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].idp",
                        CoreMatchers.equalTo(1)))  ;
    }

    @Test
    public void testarAtualizarLead() throws Exception {
        String nomeOriginal = lead.getNome();
        lead.setNome("Fabio Lever");

        Mockito.when(leadServices.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);
        String json = mapper.writeValueAsString(lead);

        /* Até aqui somente a requisição foi preparada, agora que começa o teste */
        mockMvc.perform(MockMvcRequestBuilders.put("/Lead/1").contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Fabio Lever")))
        ;

    }

    @Test
    public void testarDeletarLead() throws Exception {
        Optional<Lead> leadOptional = Optional.of(this.lead);
        Mockito.when(leadServices.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);
        String json = mapper.writeValueAsString(lead);

        /* Até aqui somente a requisição foi preparada, agora que começa o teste */
        mockMvc.perform(MockMvcRequestBuilders.delete("/Lead/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].idp", CoreMatchers.equalTo(1))) /*compara resposta JSON com o o que eu programar */ ;

        Mockito.verify(leadServices,Mockito.times(1)).deletarLead(Mockito.any(Lead.class));
    }

    @Test
    public void testarBuscarLead() throws Exception{
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadServices.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);

        Iterable<Lead> leadIterable = Arrays.asList(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/Lead/1")).
                andExpect(MockMvcResultMatchers.status().isOk());

        /*Falha não está funcionando*/
        mockMvc.perform(MockMvcRequestBuilders.get("/Lead/3"))
                .andExpect(MockMvcResultMatchers.status().isOk());
   }

    @Test
    public void testarBuscarTodosLead() throws Exception{
        Iterable<Lead> leadIterable = Arrays.asList(lead);
        Mockito.when(leadServices.buscarTodosLeads()).thenReturn(leadIterable);


        mockMvc.perform(MockMvcRequestBuilders.get("/Lead/")).
                andExpect(MockMvcResultMatchers.status().isOk());

        /*Falha não está funcionando*/
        /*mockMvc.perform(MockMvcRequestBuilders.get("/Lead/3"))
                .andExpect(MockMvcResultMatchers.status().isOk());     */
    }
}
