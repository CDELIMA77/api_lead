package com.itau.lead.collector.configurations;

import com.itau.lead.collector.models.Usuario;
import com.itau.lead.collector.security.FiltroAutenticacaoJWT;
import com.itau.lead.collector.security.FiltroAutorizacaoJWT;
import com.itau.lead.collector.security.JWTUtil;
import com.itau.lead.collector.services.UsuarioServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UsuarioServices usuarioServices;

    @Autowired
    private JWTUtil jwtUtil;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/Lead/**" /*Qdo coloca ** é para liberar os JSON depois como /1*/
           /* Produto como não está aqui, terá que gerar token e autenticar */
    };

    /* O que colocar libera o acesso, o que estiver fora estará bloqueado e só entra com autenticação */
    private static final String[] PUBLIC_MATCHERS_POST = {
            "/Lead",
            "/Produto",
            "/usuario/**"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /* Como e post, se nao coloca isso ira barra por ataque csrf, para nao trafegar os dados, precisa desabilitar */
        /* se nao tem cors é assim, porém se tem é como abaixo : http.csrf().disable(); */
        http.cors();
        http.csrf().disable();

        /* Para todo mundo que mandar get para os endereços acima deixarei aberto pra acesso, o resto estará bloqueado
            e precisará fazer login e senha no sistema  */
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                /* pode colocar o delete e o PUT */
                .anyRequest().authenticated();

        /* Precisa informar que somos uma aplicação stateless*/
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(new FiltroAutenticacaoJWT(jwtUtil, authenticationManager()));
        http.addFilter(new FiltroAutorizacaoJWT(authenticationManager(), usuarioServices, jwtUtil));

        /*super.configure(http); */
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usuarioServices).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean /*Serve para estanciar e deixar ele disponível para o Spring Security conseguir capturar e pegar as configurações*/
    CorsConfigurationSource corsConfigurationSource(){
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        /*Quais URL estao disponíveis para acesso de outros dominios*/
        source.registerCorsConfiguration("/**",new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
}