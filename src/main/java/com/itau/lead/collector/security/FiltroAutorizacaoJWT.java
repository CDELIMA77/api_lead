package com.itau.lead.collector.security;

import com.itau.lead.collector.services.UsuarioServices;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroAutorizacaoJWT extends BasicAuthenticationFilter {

    private UsuarioServices usuarioServices;

    private JWTUtil jwtUtil;

    public FiltroAutorizacaoJWT(AuthenticationManager authenticationManager, UsuarioServices usuarioServices, JWTUtil jwtUtil) {
        super(authenticationManager);
        this.usuarioServices = usuarioServices;
        this.jwtUtil = jwtUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String tokenHeader = request.getHeader("Authorization");
        if ( tokenHeader != null && tokenHeader.startsWith("Bearer ")){
            UsernamePasswordAuthenticationToken auth = getAuthentication(request, tokenHeader.substring(7));
            if ( auth != null ) {
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, String token) {
    if (jwtUtil.tokenValido(token)){
        String username = jwtUtil.getUsername(token);
        UserDetails user = usuarioServices.loadUserByUsername(username);
        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        }
    return null;
    }
}
