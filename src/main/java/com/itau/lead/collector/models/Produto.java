package com.itau.lead.collector.models;

import javax.persistence.*;

@Entity
@Table(name = "produto_cynthia")
public class Produto {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer idp;

        @Column(name = "nome_produto")
        private String nome;
        private String descricao;
        private double preco;

        public Produto() {
        }

    public Produto(Integer idp, String nome, String descricao, double preco) {
        this.idp = idp;
        this.nome = nome;
        this.descricao = descricao;
        this.preco = preco;
    }

    public Integer getIdp() {
        return idp;
    }

    public void setIdp(Integer idp) {
        this.idp = idp;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
