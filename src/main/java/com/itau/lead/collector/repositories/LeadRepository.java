package com.itau.lead.collector.repositories;

import com.itau.lead.collector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

}
