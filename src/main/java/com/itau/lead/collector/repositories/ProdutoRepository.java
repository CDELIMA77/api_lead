package com.itau.lead.collector.repositories;

import com.itau.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
