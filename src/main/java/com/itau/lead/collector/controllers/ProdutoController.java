package com.itau.lead.collector.controllers;

import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.services.ProdutoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.Optional;

@RestController
@RequestMapping("/Produto")

public class ProdutoController {

    @Autowired
    private ProdutoServices produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(){
        return produtoService.buscarTodosProdutos();
    }

    @GetMapping("/{idp}")
    public Produto buscarProduto(@PathVariable Integer idp){
        Optional<Produto> produtoOptional = produtoService.buscarPorIdp(idp);

        if (produtoOptional.isPresent()){
            return produtoOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Produto> incluirProduto(@RequestBody Produto produto) {
        Produto produtoObjeto = produtoService.salvarProduto(produto);
        return ResponseEntity.status(201).body(produtoObjeto);
    }

    @PutMapping("/{idp}")
    public ResponseEntity<Produto> atualizarProduto(@PathVariable Integer idp, @RequestBody Produto produto){
        produto.setIdp(idp);
        Produto produtoObjeto = produtoService.atualizarProduto(produto);
        return ResponseEntity.status(200).body(produtoObjeto);
    }

    @DeleteMapping("/{idp}")
    public ResponseEntity<Produto> deletarProduto(@PathVariable Integer idp){
        Optional<Produto> produtoOptional = produtoService.buscarPorIdp(idp);
        if (produtoOptional.isPresent()) {
            produtoService.deletarProduto(produtoOptional.get());
            return ResponseEntity.status(204).body(produtoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}