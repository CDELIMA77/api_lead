package com.itau.lead.collector.controllers;

import com.itau.lead.collector.models.Lead;
import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.services.LeadServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Lead")

    public class LeadController {

        @Autowired
        private LeadServices leadService;

        @GetMapping
        public Iterable<Lead> buscarTodosLeads(){
            return leadService.buscarTodosLeads();
        }

        @GetMapping("/{id}")
        public Lead buscarLead(@PathVariable Integer id){
            Optional<Lead> leadOptional = leadService.buscarPorId(id);

            if (leadOptional.isPresent()){
                return leadOptional.get();
            } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }

        }

        @PostMapping
        public ResponseEntity<Lead> incluirLead(@RequestBody @Valid Lead lead) {
            List<Integer> produtosId = new ArrayList<>();
            for (Produto produto: lead.getProdutos()){
                produtosId.add(produto.getIdp());
            }

            Iterable<Produto> produtosIterable = leadService.buscarTodosProdutos(produtosId);

            lead.setProdutos((List) produtosIterable);

            Lead leadObjeto = leadService.salvarLead(lead);
            return ResponseEntity.status(201).body(leadObjeto);
         }

         @PutMapping("/{id}")
         public ResponseEntity<Lead> atualizarLead(@PathVariable Integer id, @RequestBody @Valid Lead lead){
            lead.setId(id);
            try{
                Lead leadObjeto = leadService.atualizarLead(lead);
                return ResponseEntity.status(200).body(leadObjeto);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
             }
         }

         @DeleteMapping("/{id}")
         public ResponseEntity<Lead> deletarLead(@PathVariable Integer id){
              Optional<Lead> leadOptional = leadService.buscarPorId(id);
              if (leadOptional.isPresent()) {
                  leadService.deletarLead(leadOptional.get());
                  return ResponseEntity.status(204).body(leadOptional.get());
             }
             throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }

     }


            /* Versão com Lista

            Lead lead;

            try{
                lead = leadService.buscarPorIndice(indice);
            } catch (Exception e){
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "O indice não existe");
            }
            return lead;
        }

        @PostMapping
        public ResponseEntity<Lead> incluir(@RequestBody Lead lead) {
            Lead leadObjeto = leadService.adicionar(lead);
            return ResponseEntity.status(201).body(leadObjeto);
        }
    }
    */