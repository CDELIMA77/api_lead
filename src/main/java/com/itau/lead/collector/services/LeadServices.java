package com.itau.lead.collector.services;

import com.itau.lead.collector.models.Lead;
import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.repositories.LeadRepository;
import com.itau.lead.collector.repositories.ProdutoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadServices {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtoId){
        Iterable<Produto> produtosIterable = produtoRepository.findAllById(produtoId);
        return produtosIterable;
    }

    public Optional<Lead> buscarPorId(int id) {
       Optional<Lead> leadOptional = leadRepository.findById(id);
       return leadOptional;
    }

    public Lead salvarLead(Lead lead) {
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Lead atualizarLead(Lead lead) throws ObjectNotFoundException {
        Optional<Lead> leadOptional = buscarPorId(lead.getId());
        if (leadOptional.isPresent()){
            Lead leadData = leadOptional.get();

            if (lead.getNome() == null) {
               lead.setNome(leadData.getNome());
            }

            if (lead.getEmail() == null) {
                lead.setEmail(leadData.getEmail());
            }

            if (lead.getTipoDeLead() == null) {
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
            Lead leadObjeto = leadRepository.save(lead);
            return leadObjeto;
        }
        throw new ObjectNotFoundException(Lead.class, "O Lead não foi encontrado");
    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
        }
  }


   /* versao com lista
        private List<Lead> leads = new ArrayList(Arrays.asList(
                new Lead("Cynthia", "cynthiacarvalholima@gmail.com", TipoDeLead.QUENTE)));

        public Lead buscarPorIndice(int indice){
            Lead lead = leads.get(indice);
            return lead;
        }

        public Lead adicionar(Lead lead) {
            leads.add(lead);
            return lead;
        }
         */
