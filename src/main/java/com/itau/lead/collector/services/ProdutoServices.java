package com.itau.lead.collector.services;

import com.itau.lead.collector.models.Produto;
import com.itau.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoServices {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Optional<Produto> buscarPorIdp(int idp) {
        Optional<Produto> produtoOptional = produtoRepository.findById(idp);
        return produtoOptional;
    }

    public Produto salvarProduto(Produto produto) {
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodosProdutos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto atualizarProduto(Produto produto){
        Optional<Produto> produtoOptional = buscarPorIdp(produto.getIdp());
        if (produtoOptional.isPresent()){
            Produto produtoData = produtoOptional.get();

            if (produto.getNome() == null) {
                produto.setNome(produtoData.getNome());
            }

            if (produto.getDescricao() == null) {
                produto.setDescricao(produtoData.getDescricao());
            }

            if (produto.getPreco() == 0 ) {
                produto.setPreco(produtoData.getPreco());
            }

        }
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public void deletarProduto(Produto produto){
        produtoRepository.delete(produto);
    }
}