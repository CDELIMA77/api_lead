package com.itau.lead.collector.enums;

public enum TipoDeLead {
        QUENTE,
        ORGANICO,
        FRIO,
    }
